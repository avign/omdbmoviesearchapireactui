import "./App.css";
import MovieCard from "./components/MovieCard/MovieCard";
import SearchBar from "./components/SearchBar/SearchBar";
import React, { useEffect, useState } from "react";

const App = () => {
  const [movies, setMovies] = useState([]);
  const [moviesData, setMoviesData] = useState([]);

  useEffect(() => {
    getMoviesByTitle("war").then((res) => {
      setMovies(res);
      setMoviesData(res);
    });
  }, []);

  const getMoviesByTitle = (title) => {
    return fetch(`https://www.omdbapi.com/?apikey=45f0782a&s=${title}`)
      .then((res) => res.json())
      .then((res) => res.Search);
  };

  const handleInputChange = (text) => {
    const filtered_movies = moviesData.filter(
      (movie) =>
        movie.Title.toUpperCase().includes(text.trim().toUpperCase()) ||
        movie.Year.toUpperCase().includes(text.trim().toUpperCase())
    );
    this.setState({ movies: filtered_movies });
  };

  const handleInputChangeServer = (text) => {
    getMoviesByTitle(text).then((res) => {
      if (res) {
        setMovies(res);
        setMoviesData(res);
      }
    });
  };

  return (
    <div className="App">
      <div className="Search">
        <SearchBar onInputChange={handleInputChangeServer}></SearchBar>
      </div>
      <div className="movies">
        {movies.map((movie) => (
          <MovieCard key={movie.imdbID} {...movie} />
        ))}
      </div>
    </div>
  );
};

export default App;
