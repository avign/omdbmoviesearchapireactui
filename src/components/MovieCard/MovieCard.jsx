import React from "react";
import "./MovieCard.css";

const MovieCard = ({ Title: title, Year: year, Poster: poster_url }) => {
  return (
    <div className="movie-card">
      <div className="movie--image-container">
        <img src={poster_url} />
      </div>
      <div className="movie--details-container">
        <div className="seperator-line"></div>
        <p className="movie--title">{title}</p>
      </div>
    </div>
  );
};

export default MovieCard;
