import React from "react";
import "./SearchBar.css";
const SearchBar = ({ onInputChange }) => {
  return (
    <div className="SearchInput">
      <input
        placeholder="Type to Search"
        onChange={(e) => onInputChange(e.target.value)}
      />
    </div>
  );
};

export default SearchBar;
